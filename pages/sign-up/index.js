import { useCallback, useState } from "react";
import { Col, Container, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconButton, Input, InputAdornment, InputLabel, TextField, FormControl } from "@material-ui/core";
import { Visibility, VisibilityOff } from '@material-ui/icons';
import Link from 'next/link';

// Syles
import styles from './SignUp.module.css';

// Components
import Background from '../../components/Background/Background';
import DialogTitle from "../../components/DialogTitle/DialogTitle";

const SignIn = () => {

    const [showPassword, setShowPassword] = useState(false);
    const [showConfirm, setShowConfirm] = useState(false);

    const handleMouseDownPassword = useCallback((event) => {
        event.preventDefault();
    }, []);

    return (
        <Background background={true} showContinue={false}>
            <Container className='centered'>
                <Row className='h-100 align-items-center'>
                    <Col xs={11} md={7} lg={5} className={styles.cardContainer}>

                        <Row>
                            <DialogTitle title='Registrarte' />

                            <Col xs={12} className='my-4'>
                                <TextField
                                    // error={titleError}
                                    fullWidth
                                    label="Usuario"
                                    placeholder='Ingrese el nombre del usuario'
                                // value={title}
                                // onChange={e => {
                                //     setTitle(e.target.value);
                                //     if (e.target.value) {
                                //         setTitleError(false);
                                //     } else {
                                //         setTitleError(true);
                                //     }
                                // }}
                                // helperText={titleError && 'This field is requiered.'}
                                />
                            </Col>
                            <Col xs={12}>
                                <FormControl fullWidth>
                                    <InputLabel>Contraseña</InputLabel>
                                    <Input
                                        // error={titleError}
                                        fullWidth
                                        label="Nombre del usuario"
                                        placeholder='Ingrese la contraseña'
                                        type={showPassword ? 'text' : 'password'}
                                        // value={title}
                                        // onChange={e => {
                                        //     setTitle(e.target.value);
                                        //     if (e.target.value) {
                                        //         setTitleError(false);
                                        //     } else {
                                        //         setTitleError(true);
                                        //     }
                                        // }}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={() => setShowPassword(!showPassword)}
                                                    onMouseDown={handleMouseDownPassword}
                                                >
                                                    {showPassword ? <Visibility /> : <VisibilityOff />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    // helperText={titleError && 'This field is requiered.'}
                                    />
                                </FormControl>
                            </Col>
                            <Col xs={12} className='mt-4'>
                                <FormControl fullWidth>
                                    <InputLabel>Confirmar contraseña</InputLabel>
                                    <Input
                                        // error={titleError}
                                        fullWidth
                                        label="Nombre del usuario"
                                        placeholder='Ingrese de nuevo la contraseña'
                                        type={showConfirm ? 'text' : 'password'}
                                        // value={title}
                                        // onChange={e => {
                                        //     setTitle(e.target.value);
                                        //     if (e.target.value) {
                                        //         setTitleError(false);
                                        //     } else {
                                        //         setTitleError(true);
                                        //     }
                                        // }}
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    aria-label="toggle password visibility"
                                                    onClick={() => setShowConfirm(!showConfirm)}
                                                    onMouseDown={handleMouseDownPassword}
                                                >
                                                    {showConfirm ? <Visibility /> : <VisibilityOff />}
                                                </IconButton>
                                            </InputAdornment>
                                        }
                                    // helperText={titleError && 'This field is requiered.'}
                                    />
                                </FormControl>
                            </Col>
                            <Col xs={12} className='mt-5 text-right'>

                                <button type='button' className='btn btn-primary rounded-0'>
                                    <span>Sign up</span>
                                    <FontAwesomeIcon className='ml-2' icon={['fas', 'sign-in-alt']} />
                                </button>

                            </Col>
                            <Col xs={12} className='mt-2 text-center'>
                                <Link href='/'>¿Ya tienes cuenta?</Link>
                            </Col>
                        </Row>

                    </Col>
                </Row>
            </Container>
        </Background>
    );
}

export default SignIn;