import { Col, Container, Row } from "react-bootstrap";

import styles from '../styles/Home.module.css';

// Components
import Background from '../components/Background/Background';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

import allActions from '../redux/actions';
import httpRequestHandler from '../httpRequest';
import DeleteDialog from '../components/DeleteDialog/DeleteDialog';
import { useState } from "react";
import Head from "next/head";

const Home = () => {

  const router = useRouter();

  const interviews = useSelector(state => state.interviewReducer.interviews);
  const dispatch = useDispatch();

  const [dialog, setDialog] = useState({ id: null, open: false });

  const redirectToPath = ({ id, interviewers, candidate, skills, questions, date }) => {

    if (interviewers.length === 0) {
      router.push({ pathname: 'select-profile', query: { id } });

    } else if (!candidate || skills.length === 0) {
      router.push({ pathname: 'candidate', query: { id } });

    } else if (questions.length === 0) {
      router.push({ pathname: 'interview', query: { id } });

    } else {
      router.push({ pathname: 'resume', query: { id } });
    }

  };

  const createInterview = async () => {

    dispatch(allActions.loadingActions.startLoader());

    try {
      const interview = await httpRequestHandler('add-interview', 'POST');
      dispatch(allActions.interviewActions.addInterview(interview));
      router.push({ pathname: '/select-profile', query: { id: interview.id } });
      dispatch(allActions.loadingActions.stopLoader());
    } catch (error) {
      dispatch(allActions.loadingActions.stopLoader());
    }

  };

  const deleteInterview = async (interviewID) => {
    dispatch(allActions.loadingActions.startLoader());
    try {
      await httpRequestHandler(`delete-interview/${interviewID}`, 'DELETE');
      dispatch(allActions.interviewActions.deleteInterview(interviewID));
      setDialog({ id: null, open: false });
      dispatch(allActions.loadingActions.stopLoader());
    } catch (error) {
      dispatch(allActions.loadingActions.stopLoader());
      console.error(error);
    }
  };

  return (
    <Background title='Historial'>
      <Head>
        <title>Entrevistas</title>
      </Head>
      {
        interviews.length > 0 ? (

          <Container className='pb-5'>
            <Row>

              {
                interviews.map(({ id, interviewers, candidate, skills, questions, date }, index) => {
                  return (
                    <Col xs={12} sm={6} md={4} className='mt-4 pl-4 pr-4' key={index}>
                      <div
                        className={styles.shadowHover}
                        onClick={() => redirectToPath({ id, interviewers, candidate, skills, questions, date })}
                      >

                        <div className={styles.hiddenButton}>
                          <button
                            type='button'
                            className='btn btn-danger rounded-circle'
                            onClick={(event) => {
                              event.stopPropagation();
                              setDialog({ id, open: true })
                            }}
                          >
                            <FontAwesomeIcon icon={['fas', 'trash-alt']} />
                          </button>
                        </div>

                        <Row className='text-center'>
                          <Col xs={12}>
                            <FontAwesomeIcon style={{ fontSize: '5rem', color: '#6700ad' }} icon={['fas', 'book']} />
                          </Col>
                          <Col xs={12} className='mt-3'>
                            <span className={styles.name}>{candidate ? candidate.name : 'Nuevo candidato'}</span>
                          </Col>
                          <Col xs={12} className='mt-1'>

                            {

                              skills ? skills.map((skill, index) => {

                                if (skill.active) {
                                  return (
                                    <span key={index} className='text-break'>{skill.name} </span>
                                  )
                                }

                              }) : ''
                            }

                          </Col>
                          <Col xs={12} className='mt-1'>
                            <span className={styles.date}>{new Date(date).toLocaleDateString('es-MX', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })}</span>
                          </Col>

                          <Col xs={12} className='mt-1'>
                            <span style={{ color: '#6700ad' }}>
                              {questions.length > 0 ? 'Ver resultados' : 'Continuar con la entrevista'}
                            </span>
                          </Col>

                        </Row>

                      </div>
                    </Col>
                  )
                })
              }

              <Col xs={12} sm={6} md={4} className='mt-4 pl-4 pr-4' style={{ minHeight: '16rem' }}>
                <div
                  className={`${styles.shadowHover} d-flex align-items-center`}
                  onClick={createInterview}
                >
                  <Row className='text-center'>
                    <Col xs={12}>
                      <div>
                        <FontAwesomeIcon className={styles.titleIcon} icon={['fas', 'book']} />
                        <FontAwesomeIcon className={styles.plusIcon} icon={['fas', 'plus']} />
                      </div>
                    </Col>
                    <Col xs={12} className='mt-4 ml-auto mr-auto' style={{ maxWidth: '13rem' }}>
                      <span>Haz click aquí para iniciar nueva entrevista</span>
                    </Col>
                  </Row>
                </div>
              </Col>

            </Row>

            <DeleteDialog info={dialog} handleClose={setDialog} confirm={(id) => deleteInterview(id)} />

          </Container>

        ) :
          (
            <Container className='paddingBottom centered'>
              <Row className='h-100 d-flex align-items-center'>
                <Col xs={12}>
                  <Row
                    className='text-center'
                    style={{ cursor: 'pointer' }}
                    onClick={createInterview}
                  >
                    <Col xs={12}>
                      <span style={{ fontSize: '1.7rem' }}>No se han registrado ninguna entrevista</span>
                    </Col>
                    <Col xs={12} className='my-4'>
                      <div>
                        <FontAwesomeIcon className={styles.titleIcon} icon={['fas', 'book']} />
                        <FontAwesomeIcon className={styles.plusIcon} icon={['fas', 'plus']} />
                      </div>
                    </Col>
                    <Col xs={12}>
                      <span style={{ fontSize: '1.3rem' }}>Haz click aquí para añadir</span>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Container>
          )
      }

    </Background >
  );
}

export default Home;