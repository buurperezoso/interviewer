import { useEffect, useRef, useState } from "react";
import { TextField } from "@material-ui/core";
import { Row, Col, Container } from "react-bootstrap";
import { useRouter } from "next/router";
import Head from 'next/head'

// Styles
import styles from './Resume.module.css';

// Redux
import { useDispatch, useSelector } from "react-redux";
import allActions from '../../redux/actions';

// Http requests
import httpRequestHandler from '../../httpRequest';

// Components
import Background from "../../components/Background/Background";
import DisplayQuestionsDialog from "../../components/DisplayQuestionsDialog/DisplayQuestionsDialog";



const Resume = () => {

    const router = useRouter();
    const interviews = useSelector(state => state.interviewReducer.interviews);
    const dispatch = useDispatch();
    const timer = useRef();

    const [openDialog, setOpenDialog] = useState(false);

    const [interview, setInterview] = useState(null);

    const [comments, setComments] = useState('');

    const [candidateName, setCantidateName] = useState('');

    useEffect(() => {
        const { id } = router.query;
        const interviewFound = interviews.find((interview) => interview.id == id);

        if (interviewFound) {
            setCantidateName(interviewFound.candidate.name);
            setInterview(interviewFound);
        }

        if (interviewFound && interviewFound.comments) {
            setComments(interviewFound.comments);
        }

    }, [interviews]);

    const finishInterview = () => {
        router.push('/');
    };

    const handleComments = (value) => {
        setComments(value);

        if (timer) {
            clearTimeout(timer.current);
        }

        const { id } = router.query;
        timer.current = setTimeout(async () => {
            try {
                const response = await httpRequestHandler(`add-comments/${id}`, 'POST', value);
                dispatch(allActions.interviewActions.finishInterview(response, id));
            } catch (error) {
                console.error(error);
            }
        }, 2000);
    }

    return (
        <Background
            title='Resumen'
            disabled={false}
            showContinue={true}
            label='Finalizar'
            clickContinue={finishInterview}
        >
            <Head>
                <title>Información de entrevista de {candidateName}</title>
            </Head>

            {
                interview ? (<Container className='paddingBottom'>
                    <Row>
                        <Col md={6}>
                            <Row>
                                <Col xs={12} className='mt-4'>
                                    <span className={styles.title}>Datos del candidato</span>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12} className='mt-3'>
                                    <span className={styles.grayTitle}>Nombre completo</span>
                                </Col>
                                <Col xs={12}>
                                    <span style={{ fontSize: '1.1rem' }}>{interview.candidate.name}</span>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12} className='mt-3'>
                                    <span className={styles.grayTitle}>Correo electrónico</span>
                                </Col>
                                <Col xs={12}>
                                    <span style={{ fontSize: '1.1rem' }}>{interview.candidate.email}</span>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12} className='mt-3'>
                                    <span className={styles.grayTitle}>Tipo</span>
                                </Col>
                                <Col xs={12}>
                                    <span style={{ fontSize: '1.1rem' }}>{interview.candidate.type}</span>
                                </Col>
                            </Row>
                        </Col>
                        <Col md={6}>
                            <Row>
                                <Col xs={6} className='mt-4'>
                                    <span className={styles.title}>Resultados</span>
                                </Col>
                                <Col xs={6} className='text-center mt-4 d-flex align-items-center justify-content-center'>
                                    <a className={styles.showQuestions} onClick={() => setOpenDialog(true)}>Ver preguntas</a>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={6} className='mt-3'>
                                    <Row>
                                        <Col xs={12}>
                                            <span className={styles.grayTitle}>Skill</span>
                                        </Col>

                                        {
                                            interview.skills.map((skill, index) => {
                                                if (skill.active) {
                                                    return (
                                                        <Col xs={12} key={index}>
                                                            <span>{skill.name}</span>
                                                        </Col>
                                                    )
                                                }
                                            })
                                        }
                                    </Row>
                                </Col>
                                <Col xs={6} className='text-center mt-3'>
                                    <Row>
                                        <Col xs={12}>
                                            <span className={styles.grayTitle}>Puntaje</span>
                                        </Col>
                                        {
                                            interview.skills.map((skill, index) => {
                                                if (skill.active) {
                                                    const questionsCorrect = interview.questions.filter((question) => question.subject === skill.name && question.answer === 'correct');
                                                    return (
                                                        <Col xs={12} key={index}>
                                                            <span>{questionsCorrect ? questionsCorrect.length : '0'}/5</span>
                                                        </Col>
                                                    )
                                                }
                                            })
                                        }
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>

                    <Row>
                        <Col md={6}>
                            <Row>
                                <Col xs={12} className='mt-4'>
                                    <span className={styles.title}>Comentarios:</span>
                                </Col>
                                <Col xs={12} className='mt-3'>
                                    <TextField
                                        value={comments}
                                        fullWidth
                                        multiline
                                        rows={6}
                                        variant="outlined"
                                        onChange={e => {
                                            handleComments(e.target.value);
                                        }}
                                    />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>) : null
            }

            <DisplayQuestionsDialog open={openDialog} handleClose={setOpenDialog} />
        </Background>
    );
}

export default Resume;