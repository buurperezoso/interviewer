import { Fragment, useCallback, useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Col, Container, Row } from 'react-bootstrap';
import { useRouter } from 'next/router';
import Head from 'next/head';

// Styles
import styles from './SelectProfile.module.css';

// Redux
import { useDispatch, useSelector } from 'react-redux';
import allActions from '../../redux/actions';

// Http requests
import httpRequestHandler from '../../httpRequest';

// Components
import Background from '../../components/Background/Background';
import InterviewerCard from '../../components/InterviewerCard/InterviewerCard';
import AddInterviewerDialog from '../../components/AddInterviewerDialog/AddInterviewerDialog';
import DeleteDialog from '../../components/DeleteDialog/DeleteDialog';

const SelectProfile = ({ profilesProps }) => {
    const profiles = useSelector(state => state.profilesReducer.profileList);
    const dispatch = useDispatch();

    const interviews = useSelector(state => state.interviewReducer.interviews);

    const router = useRouter();

    const [openDialog, setOpenDialog] = useState({
        open: false,
        profile: null
    });

    const [deleteDialog, setDeleteDialog] = useState({
        id: null,
        open: false
    });

    const [selected, setSelected] = useState([]);

    useEffect(() => {
        dispatch(allActions.profileActions.setProfileList(profilesProps));
        const { id } = router.query;

        const interview = interviews.find((interview) => interview.id == id);

        if (interview && 'interviewers' in interview) {
            setSelected(interview.interviewers);
        }

    }, [interviews]);

    const handleSelected = (profile) => {

        const findID = selected.find((element) => element.id === profile.id);

        if (findID) { // Find the selected profile in the array, if it does, remove it from array, else add it
            setSelected(selected.filter((element) => element.id !== profile.id));
        } else {
            setSelected([...selected, profile]);
        }
    };

    const createInterview = async () => {

        const { id } = router.query;

        dispatch(allActions.loadingActions.startLoader());

        try {
            const response = await httpRequestHandler(`add-interviewers/${id}`, 'POST', selected);
            dispatch(allActions.interviewActions.addSelectedProfiles(response, id));
            router.push({
                pathname: '/candidate',
                query: { id }
            });
            dispatch(allActions.loadingActions.stopLoader());
        } catch (error) {
            dispatch(allActions.loadingActions.stopLoader());
            console.error(error);
        }

    };

    const handleDeleteProfile = async (id) => {
        dispatch(allActions.loadingActions.startLoader());
        try {
            const response = await httpRequestHandler(`delete-profile/${id}`, 'DELETE');
            dispatch(allActions.profileActions.deleteProfile(id));

            const filteredSelected = selected.filter((element) => element.id !== id); // Delete the profile in the
            setSelected(filteredSelected ? filteredSelected : []);                    // local array too if it in

            setDeleteDialog({ id: null, open: false });
            dispatch(allActions.loadingActions.stopLoader());
        } catch (error) {
            console.error(error);
            dispatch(allActions.loadingActions.stopLoader());
        }

    };

    const handleEditProfile = (profile) => {
        setOpenDialog({
            open: true,
            profile
        });
    };

    return (
        <Background
            title='Entrevistadores'
            disabled={selected.length === 0 ? true : false}
            clickContinue={() => createInterview(selected)}
            showContinue={true}
            backButton={() => router.push('/')}
        >
            <Head>
                <title>Selecciona a los entrevistadores</title>
            </Head>

            {
                profiles.length > 0

                    ? <Container className='paddingBottom'>
                        <Row>
                            {
                                profiles.map(({ id, name, employeeID, enterpriseID }) => {
                                    return (
                                        <Col xs={12} sm={6} md={4} lg={3} className='mt-5 pl-4 pr-4' key={id}>
                                            <div
                                                className={styles.shadowHover}
                                                style={{
                                                    border: selected.find((element) => element.id === id) && '1px solid #9e9e9e79',
                                                    boxShadow: selected.find((element) => element.id === id) && '0 0.4rem 0.3rem #9E9E9E'
                                                }}
                                                onClick={() => handleSelected({ id, name, employeeID, enterpriseID })}
                                            >
                                                <div className={styles.hiddenButton}>
                                                    <button
                                                        type='button'
                                                        className='btn btn-danger rounded-circle'
                                                        onClick={(event) => {
                                                            event.stopPropagation();
                                                            setDeleteDialog({ id: id, open: true });
                                                        }}
                                                    >
                                                        <FontAwesomeIcon icon={['fas', 'trash-alt']} />
                                                    </button>
                                                    <button
                                                        type='button'
                                                        className='btn btn-primary rounded-circle ml-2'
                                                        onClick={(event) => {
                                                            event.stopPropagation();
                                                            handleEditProfile({ id, name, employeeID, enterpriseID });
                                                        }}
                                                    >
                                                        <FontAwesomeIcon icon={['fas', 'pen']} />
                                                    </button>
                                                </div>
                                                <InterviewerCard
                                                    name={name}
                                                    employeeID={employeeID}
                                                    enterpriseID={enterpriseID}
                                                />
                                            </div>
                                        </Col>
                                    )
                                })
                            }


                            <Col xs={12} sm={6} md={4} lg={3} className='mt-5 pl-4 pr-4'>
                                <div
                                    className={`${styles.shadowHover} d-flex align-items-center`}
                                    onClick={() => {
                                        setOpenDialog({
                                            open: true,
                                            profile: null
                                        });
                                    }}
                                >
                                    <Row className='text-center'>
                                        <Col xs={12}>
                                            <div>
                                                <FontAwesomeIcon className={styles.titleIcon} icon={['far', 'user']} />
                                                <FontAwesomeIcon className={styles.plusIcon} icon={['fas', 'plus']} />
                                            </div>
                                        </Col>
                                        <Col xs={12} className='mt-3'>
                                            <span>Haz click aquí para añadir a otro entrevistador</span>
                                        </Col>
                                    </Row>
                                </div>
                            </Col>

                        </Row>

                    </Container>

                    : <Container className='paddingBottom centered'>
                        <Row className='h-100 d-flex align-items-center'>
                            <Col xs={12}>
                                <Row
                                    className='text-center'
                                    style={{ cursor: 'pointer' }}
                                    onClick={() => {
                                        setOpenDialog({
                                            open: true,
                                            profile: null
                                        });
                                    }}
                                >
                                    <Col xs={12}>
                                        <span style={{ fontSize: '1.7rem' }}>No se han registrado ningún entrevistador</span>
                                    </Col>
                                    <Col xs={12} className='my-4'>
                                        <div>
                                            <FontAwesomeIcon className={styles.titleIcon} icon={['far', 'user']} />
                                            <FontAwesomeIcon className={styles.plusIcon} icon={['fas', 'plus']} />
                                        </div>
                                    </Col>
                                    <Col xs={12}>
                                        <span style={{ fontSize: '1.3rem' }}>Haz click aquí para añadir</span>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Container>
            }

            <AddInterviewerDialog
                info={openDialog}
                handleClose={setOpenDialog}
            />

            <DeleteDialog info={deleteDialog} handleClose={setDeleteDialog} confirm={(id) => handleDeleteProfile(id)} />

        </Background>
    );
}

export async function getStaticProps() { // Fetch the data from the profiles db and send it as param to component
    const profiles = await httpRequestHandler('get-profiles', 'GET');
    return {
        props: {
            profilesProps: profiles,
        },
    }
}

export default SelectProfile;