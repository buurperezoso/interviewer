import { useCallback, useEffect, useRef, useState } from "react";
import { Carousel, Col, Container, Row } from "react-bootstrap";
import { useRouter } from "next/router";
import { FormControl, RadioGroup, FormControlLabel, Radio, TextField } from "@material-ui/core";

// Styles
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import styles from './Interview.module.css';

// Redux
import { useDispatch, useSelector } from "react-redux";
import allActions from '../../redux/actions';

// Http requests
import httpRequestHandler from '../../httpRequest';

// Components
import Background from "../../components/Background/Background";

// Constants
import { questionsPreset } from '../../constants/constants';
import Head from "next/head";

const Interview = () => {

    const [questions, setQuestions] = useState([]);

    const timer = useRef(null);

    const interviews = useSelector(state => state.interviewReducer.interviews);
    const dispatch = useDispatch();

    const router = useRouter();

    const [index, setIndex] = useState(0); // Index of question shown in carousel

    const [candidateName, setCandidateName] = useState('');

    const handleSelect = useCallback((selectedIndex, e) => {
        setIndex(selectedIndex);
    }, [setIndex]);

    useEffect(() => {

        let questionsArray = [];
        const { id } = router.query;
        const interview = interviews.find((interview) => interview.id == id);

        if (interview) {
            setCandidateName(interview.candidate.name);
            interview.skills.map((skill) => { // Filter the questions from the preset to the skills active
                if (skill.active) {
                    const questionsStack = questionsPreset.filter((question) => question.subject === skill.name);
                    questionsArray = [...questionsArray, ...questionsStack];
                }
            });

            setQuestions(questionsArray);
        }

    }, [interviews, router, questionsPreset]);

    const handleCorrectChange = (index, answer) => { // Updates the questions array when radio button change

        const array = [...questions];
        array[index].answer = answer;
        setQuestions(array);

    };

    const handleCommentsChange = (index, message) => { // Updates the questions array when comments are added

        if (timer) { // Replace the setTimeout process until the user stops writing for 1 second
            clearTimeout(timer.current);
        }

        timer.current = setTimeout(() => {
            const array = [...questions];
            array[index].comments = message;
            setQuestions(array);
        }, 1000);

    };

    const finishInterview = async () => {

        const { id } = router.query;

        dispatch(allActions.loadingActions.startLoader());

        try {
            const response = await httpRequestHandler(`add-questions/${id}`, 'POST', questions);
            dispatch(allActions.interviewActions.addQuestions(response, id));
            router.push({
                pathname: '/resume',
                query: { id }
            });
            dispatch(allActions.loadingActions.stopLoader());
        } catch (error) {
            dispatch(allActions.loadingActions.stopLoader());
            console.error(error);
        }

    };

    const theme = createMuiTheme({
        palette: {
            primary: { main: '#6700ad' }
        },
    });

    return (
        <Background
            title='Preguntas'
            disabled={index === questions.length - 1 ? false : true}
            showContinue={true}
            label='Finalizar'
            clickContinue={finishInterview}
        >
            <Head>
                <title>Entrevista en proceso a {candidateName}</title>
            </Head>
            <div className='paddingBottom'>

                <Carousel
                    interval={100000000000}
                    indicators={false}
                    activeIndex={index}
                    onSelect={handleSelect}
                >
                    {
                        questions.map(({ id, subject, question, answer, comments }, index) => {
                            return (

                                <Carousel.Item className='h-100' key={index}>

                                    <Container className='h-100 mt-4 mt-md-5'>
                                        <Row className='align-items-center h-100'>
                                            <Col xs={12} className='text-center'>
                                                <Row>
                                                    <Col xs={12}>
                                                        <span className={styles.subject}>{subject}</span>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col xs={12} className='mt-3'>
                                                        <span className={styles.question}>{question}</span>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col xs={12} className='mt-3'>
                                                        <MuiThemeProvider theme={theme}>
                                                            <FormControl
                                                                component="fieldset"
                                                                onChange={(e) => {
                                                                    handleCorrectChange(index, e.target.value)
                                                                }}
                                                            >
                                                                <RadioGroup row aria-label="position" name="position" defaultValue="top">
                                                                    <FormControlLabel value="correct" control={<Radio color="primary" />} label="Correcto" />
                                                                    <FormControlLabel className='ml-md-5' value="incorrect" control={<Radio color="primary" />} label="Incorrecto" />
                                                                </RadioGroup>
                                                            </FormControl>
                                                        </MuiThemeProvider>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col md={10} className='ml-auto mr-auto mt-3'>
                                                        <span className='mr-sm-3' style={{ fontSize: '1.3rem' }}>Comentarios:</span>
                                                        <TextField
                                                            className={styles.commentTextbox}
                                                            fullWidth
                                                            multiline
                                                            rows={6}
                                                            variant='outlined'
                                                            onChange={e => {
                                                                handleCommentsChange(index, e.target.value);
                                                            }}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Container>

                                </Carousel.Item>

                            )
                        })
                    }

                </Carousel>

                <Container>
                    <Row>
                        <Col xs={12} className='mt-3 text-center'>
                            <span style={{ color: '#6700ad', fontSize: '2rem', fontWeight: '500' }}>{index + 1}</span>
                            <span style={{ fontSize: '2rem', fontWeight: '500' }}>/{questions.length}</span>
                        </Col>
                    </Row>
                </Container>

            </div>

        </Background>
    );
}

export default Interview;