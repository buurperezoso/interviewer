import { useCallback, useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useRouter } from "next/router";
import { Col, Container, Row } from "react-bootstrap";

// Styles
import styles from './Candidate.module.css';

// Redux
import { useSelector } from "react-redux";

// Components
import Background from "../../components/Background/Background";
import AddSkillsDialog from "../../components/AddSkillsDialog/AddSkillsDialog";
import AddCandidateDialog from "../../components/AddCandidateDialog/AddCandidateDialog";
import SkillsContent from '../../components/SkillsContent/SkillsContent';
import NoSkillsContent from "../../components/SkillsContent/NoSkillsContent";
import Head from "next/head";


const Candidate = () => {

    const router = useRouter();
    const interviews = useSelector(state => state.interviewReducer.interviews);

    const [candidateInfo, setCandidateInfo] = useState(null);
    const [skills, setSkills] = useState([]);

    useEffect(() => {

        const { id } = router.query;

        const interview = interviews.find((interview) => interview.id == id);

        if (interview && 'candidate' in interview) {
            setCandidateInfo(interview.candidate);
        }

        if (interview && interview.skills.length > 0) {
            setSkills(interview.skills);
        }

    }, [interviews]);

    const [openSkillsDialog, setOpenSkillsDialog] = useState(false); // Skills dialog/modal
    const [openCandDialog, setOpenCandDialog] = useState(false); // Candidate dialog/modal

    const backProfiles = () => {
        const { id } = router.query;
        router.push({
            pathname: '/select-profile',
            query: { id }
        });
    };

    const startInterview = () => {
        const { id } = router.query;
        router.push({
            pathname: '/interview',
            query: { id }
        });
    };

    return (
        <Background
            title='Candidato'
            disabled={skills.filter(({ active }) => active).length === 0 || !candidateInfo}
            backButton={backProfiles}
            showContinue={true}
            clickContinue={startInterview}
            label='Comenzar'
        >
            <Head>
                <title>Agrega los datos y skills del candidato</title>
            </Head>
            {
                candidateInfo
                    ? <Container className='paddingBottom'>
                        <Row>
                            <Col xs={12} md={6} className='mt-4 mt-md-5'>

                                <div className={styles.borderTopBottom} style={{ cursor: 'pointer' }} onClick={() => setOpenCandDialog(true)}>
                                    <Row>
                                        <Col>
                                            <FontAwesomeIcon className={styles.userIcon} icon={['far', 'user']} />
                                        </Col>
                                        <Col xs={9} className='d-flex align-items-center'>
                                            <Row>
                                                <Col xs={12}>
                                                    <span className={styles.grayText}>Nombre completo</span>
                                                </Col>
                                                <Col xs={12} className='mt-2 text-break'>
                                                    <span className={styles.userText}>{candidateInfo.name}</span>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col xs={12} className='mt-4'>
                                            <span className={styles.grayText}>Correo electrónico</span>
                                        </Col>
                                        <Col xs={12} className='mt-2 text-break'>
                                            <span className={styles.userText}>{candidateInfo.email}</span>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col xs={12} className='mt-3'>
                                            <span className={styles.grayText}>Tipo</span>
                                        </Col>
                                        <Col xs={12} className='mt-2'>
                                            <span className={styles.userText}>{candidateInfo.type}</span>
                                        </Col>
                                    </Row>
                                </div>

                            </Col>
                            {
                                skills.filter(({ active }) => active).length > 0
                                    ? <SkillsContent skills={skills} openDialog={setOpenSkillsDialog} />
                                    : <NoSkillsContent openDialog={setOpenSkillsDialog} />
                            }
                        </Row>
                    </Container>

                    : <Container className='paddingBottom centered'>
                        <Row className='h-100 d-flex align-items-center'>
                            <Col xs={12}>
                                <Row
                                    className='text-center'
                                    style={{ cursor: 'pointer' }}
                                    onClick={() => setOpenCandDialog(true)}
                                >
                                    <Col xs={12}>
                                        <span style={{ fontSize: '1.7rem' }}>No se han registrado ningún candidato</span>
                                    </Col>
                                    <Col xs={12} className='my-4'>
                                        <div>
                                            <FontAwesomeIcon className={styles.titleIcon} icon={['far', 'user']} />
                                            <FontAwesomeIcon className={styles.plusIcon} icon={['fas', 'plus']} />
                                        </div>
                                    </Col>
                                    <Col xs={12}>
                                        <span style={{ fontSize: '1.3rem' }}>Haz click aquí para añadir</span>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Container>
            }

            <AddSkillsDialog open={openSkillsDialog} handleClose={setOpenSkillsDialog} />
            <AddCandidateDialog open={openCandDialog} handleClose={setOpenCandDialog} />
        </Background>
    );
}

export default Candidate;