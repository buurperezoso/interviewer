import '../styles/globals.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';

import { Provider } from 'react-redux';
import store from '../redux/store';

import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect } from 'react';

import GetInterviewsComponent from '../components/GetInterviewsComponent/GetInterviewsComponent';

import '@fortawesome/fontawesome-svg-core/styles.css';
import { config } from '@fortawesome/fontawesome-svg-core';
config.autoAddCss = false;

library.add(fas, fab, far);

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <GetInterviewsComponent>
        <Component {...pageProps} />
      </GetInterviewsComponent>
    </Provider>
  )
}

export default MyApp
