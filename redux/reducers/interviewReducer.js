import { reduxType } from '../../constants/reduxActions';

const initialState = {
    interviews: [

    ]
}

const interviewReducer = (state = initialState, action) => {

    let interviewsUpdated;

    switch (action.type) {
        case reduxType.SET_INTERVIEWS:
            return {
                ...state,
                interviews: action.payload.interviews
            };

        case reduxType.ADD_INTERVIEW:
            return {
                ...state,
                interviews: [action.payload.interview, ...state.interviews]
            }

        case reduxType.SET_SELECTED_PROFILES:

            interviewsUpdated = manageInterviewsArray(
                'interviewers',
                action.payload.selectedProfiles,
                action.payload.interviewID,
                state.interviews
            );

            return {
                ...state,
                interviews: interviewsUpdated
            };

        case reduxType.ADD_CANDIDATE:

            interviewsUpdated = manageInterviewsArray(
                'candidate',
                action.payload.candidate,
                action.payload.interviewID,
                state.interviews
            );

            return {
                ...state,
                interviews: interviewsUpdated
            };

        case reduxType.ADD_SKILLS:

            interviewsUpdated = manageInterviewsArray(
                'skills',
                action.payload.skills,
                action.payload.interviewID,
                state.interviews
            );

            return {
                ...state,
                interviews: interviewsUpdated
            };

        case reduxType.ADD_QUESTIONS:

            interviewsUpdated = manageInterviewsArray(
                'questions',
                action.payload.questions,
                action.payload.interviewID,
                state.interviews
            );

            return {
                ...state,
                interviews: interviewsUpdated
            };

        case reduxType.FINISH_INTERVIEW:

            interviewsUpdated = manageInterviewsArray(
                'comments',
                action.payload.comments,
                action.payload.interviewID,
                state.interviews
            );

            return {
                ...state,
                interviews: interviewsUpdated
            };

        case reduxType.DELETE_INTERVIEW:

            interviewsUpdated = state.interviews.filter((interview) => interview.id !== action.payload.interviewID);

            return {
                ...state,
                interviews: interviewsUpdated
            };

        default:
            return state;
    }
};

const manageInterviewsArray = (key, objectToAdd, interviewID, interviews) => {
    return interviews.map((interview) => {
        if (interview.id == interviewID) {

            interview = {
                ...interview,
                [key]: objectToAdd
            }

        }

        return interview;
    });
};

export default interviewReducer;