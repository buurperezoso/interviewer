import { reduxType } from '../../constants/reduxActions';

const initialState = {
    profileList: []
}

const profilesReducer = (state = initialState, action) => {

    switch (action.type) {
        case reduxType.SET_PROFILES:
            return {
                ...state,
                profileList: action.payload.profileList
            };

        case reduxType.ADD_PROFILE:
            return {
                ...state,
                profileList: [action.payload.profile, ...state.profileList]
            };

        case reduxType.DELETE_PROFILE:
            return {
                ...state,
                profileList: state.profileList.filter((element) => element.id !== action.payload.id)
            };

        case reduxType.UPDATE_PROFILE:
            return {
                ...state,
                profileList: state.profileList.map((element) => {
                    if (element.id === action.payload.profile.id) {
                        element = action.payload.profile;
                    }

                    return element;
                })
            }

        default:
            return state
    }
};

export default profilesReducer;