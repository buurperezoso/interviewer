import { combineReducers } from 'redux';

import profilesReducer from './profilesReducer';
import interviewReducer from './interviewReducer';
import loadingReducer from './loadingReducer';

const rootReducer = combineReducers({
    profilesReducer,
    interviewReducer,
    loadingReducer
});

export default rootReducer;