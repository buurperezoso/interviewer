import profileActions from './profileActions';
import interviewActions from './interviewActions';
import loadingActions from './loadingActions';

const allActions = {
    profileActions,
    interviewActions,
    loadingActions
}

export default allActions;