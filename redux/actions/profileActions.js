import { reduxType } from '../../constants/reduxActions';

const setProfileList = (profileList) => {
    return {
        type: reduxType.SET_PROFILES,
        payload: { profileList }
    }
}

const addProfile = (profile) => {
    return {
        type: reduxType.ADD_PROFILE,
        payload: { profile }
    }
}

const deleteProfile = (id) => {
    return {
        type: reduxType.DELETE_PROFILE,
        payload: { id }
    }
}

const updateProfile = (profile) => {
    return {
        type: reduxType.UPDATE_PROFILE,
        payload: { profile }
    }
}

const profileActions = {
    setProfileList,
    addProfile,
    deleteProfile,
    updateProfile
}

export default profileActions;