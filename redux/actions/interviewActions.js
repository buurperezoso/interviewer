import { reduxType } from '../../constants/reduxActions';

const setInterviewList = (interviews) => {
    return {
        type: reduxType.SET_INTERVIEWS,
        payload: { interviews }
    }
}

const addInterview = (interview) => {
    return {
        type: reduxType.ADD_INTERVIEW,
        payload: { interview }
    }
}

const addSelectedProfiles = (selectedProfiles, interviewID) => {
    return {
        type: reduxType.SET_SELECTED_PROFILES,
        payload: { selectedProfiles, interviewID }
    }
}

const addCandidate = (candidate, interviewID) => {
    return {
        type: reduxType.ADD_CANDIDATE,
        payload: { candidate, interviewID }
    }
}

const addSkills = (skills, interviewID) => {
    return {
        type: reduxType.ADD_SKILLS,
        payload: { skills, interviewID }
    }
}

const addQuestions = (questions, interviewID) => {
    return {
        type: reduxType.ADD_QUESTIONS,
        payload: { questions, interviewID }
    }
}

const finishInterview = (comments, interviewID) => {
    return {
        type: reduxType.FINISH_INTERVIEW,
        payload: { comments, interviewID }
    }
}

const deleteInterview = (interviewID) => {
    return {
        type: reduxType.DELETE_INTERVIEW,
        payload: { interviewID }
    }
}

const interviewActions = {
    setInterviewList,
    addSelectedProfiles,
    addCandidate,
    addSkills,
    addQuestions,
    addInterview,
    finishInterview,
    deleteInterview,
}

export default interviewActions;