import { reduxType } from '../../constants/reduxActions';

const startLoader = () => {
    return {
        type: 'LOADING',
    }
}

const stopLoader = () => {
    return {
        type: 'NOT_LOADING',
    }
}

const loadingActions = {
    stopLoader,
    startLoader,
}

export default loadingActions;