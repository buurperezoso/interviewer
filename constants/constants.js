export const api = 'https://evc02tdigb.execute-api.us-west-1.amazonaws.com/dev';

export const skillsPreset = [
    {
        name: 'Javascript',
        active: false,
    },
    {
        name: 'ReactJS',
        active: false,
    },
    {
        name: 'Angular',
        active: false,
    },
    {
        name: 'HTML',
        active: false,
    },
    {
        name: 'CSS',
        active: false,
    },
    {
        name: 'TypeScript',
        active: false,
    },
    {
        name: 'Git',
        active: false,
    },
    {
        name: 'NodeJs',
        active: false,
    },
];

export const questionsPreset = [
    {
        id: 1,
        subject: 'Javascript',
        question: '¿Cuáles son las posibles formas de crear objetos en JavaScript?',
        answer: null,
        comments: '',
    },
    {
        id: 2,
        subject: 'Javascript',
        question: '¿Cuál es la diferencia entre "slice" y "splice"?',
        answer: null,
        comments: '',
    },
    {
        id: 3,
        subject: 'Javascript',
        question: '¿Cuál es la diferencia entre los operadores == y ===?',
        answer: null,
        comments: '',
    },
    {
        id: 4,
        subject: 'Javascript',
        question: '¿Qué son las funciones lambda o flecha?',
        answer: null,
        comments: '',
    },
    {
        id: 5,
        subject: 'Javascript',
        question: '¿Qué son los "callback"?',
        answer: null,
        comments: '',
    },
    {
        id: 6,
        subject: 'ReactJS',
        question: '¿Qué es React?',
        answer: null,
        comments: '',
    },
    {
        id: 7,
        subject: 'ReactJS',
        question: '¿Qué es JSX?',
        answer: null,
        comments: '',
    },
    {
        id: 8,
        subject: 'ReactJS',
        question: '¿Cuándo usar un componente de clase sobre un "functional component"?',
        answer: null,
        comments: '',
    },
    {
        id: 9,
        subject: 'ReactJS',
        question: '¿Qué es el "state" en React?',
        answer: null,
        comments: '',
    },
    {
        id: 10,
        subject: 'ReactJS',
        question: '¿Qué son los "props" en React?',
        answer: null,
        comments: '',
    },
    {
        id: 11,
        subject: 'Angular',
        question: '¿Qué es Angular Framework?',
        answer: null,
        comments: '',
    },
    {
        id: 12,
        subject: 'Angular',
        question: '¿Qué es subjectScript?',
        answer: null,
        comments: '',
    },
    {
        id: 13,
        subject: 'Angular',
        question: '¿Qué son los componentes?',
        answer: null,
        comments: '',
    },
    {
        id: 14,
        subject: 'Angular',
        question: '¿Qué son las directivas?',
        answer: null,
        comments: '',
    },
    {
        id: 15,
        subject: 'Angular',
        question: '¿Cuales son los "lifecycle hooks" disponibles en Angular?',
        answer: null,
        comments: '',
    },
    {
        id: 16,
        subject: 'HTML',
        question: '¿Qué es el DOM? ',
        answer: null,
        comments: '',
    },
    {
        id: 17,
        subject: 'HTML',
        question: '¿Cómo funciona el DOM?',
        answer: null,
        comments: '',
    },
    {
        id: 18,
        subject: 'HTML',
        question: '¿Qué hace un <DOCsubject html>?',
        answer: null,
        comments: '',
    },
    {
        id: 19,
        subject: 'HTML',
        question: '¿Cuáles son los componentes básicos de HTML5?',
        answer: null,
        comments: '',
    },
    {
        id: 20,
        subject: 'HTML',
        question: '¿Cuáles son los beneficios del "Server Side Rendering (SSR)" sobre "Client Side Rendering (CSR)"?',
        answer: null,
        comments: '',
    },
    {
        id: 21,
        subject: 'CSS',
        question: '¿Qué es CSS?',
        answer: null,
        comments: '',
    },
    {
        id: 22,
        subject: 'CSS',
        question: '¿Cómo se puede integrar / importar CSS en una página web?',
        answer: null,
        comments: '',
    },
    {
        id: 23,
        subject: 'CSS',
        question: '¿Qué es un "Selector"?',
        answer: null,
        comments: '',
    },
    {
        id: 24,
        subject: 'CSS',
        question: '¿Qué son los "Properties"?',
        answer: null,
        comments: '',
    },
    {
        id: 25,
        subject: 'CSS',
        question: '¿Como se hace una pagina responsiva con CSS?',
        answer: null,
        comments: '',
    },
    {
        id: 26,
        subject: 'TypeScript',
        question: 'TEST',
        answer: null,
        comments: '',
    },
    {
        id: 27,
        subject: 'TypeScript',
        question: 'TEST',
        answer: null,
        comments: '',
    },
    {
        id: 28,
        subject: 'TypeScript',
        question: 'TESTTEST',
        answer: null,
        comments: '',
    },
    {
        id: 29,
        subject: 'TypeScript',
        question: 'TESTTESTTEST',
        answer: null,
        comments: '',
    },
    {
        id: 30,
        subject: 'TypeScript',
        question: 'TEST',
        answer: null,
        comments: '',
    },
    {
        id: 31,
        subject: 'Git',
        question: 'TESTTESTTEST',
        answer: null,
        comments: 'TEST',
    },
    {
        id: 32,
        subject: 'Git',
        question: 'TESTTEST',
        answer: null,
        comments: '',
    },
    {
        id: 33,
        subject: 'Git',
        question: 'TESTTESTTEST',
        answer: null,
        comments: '',
    },
    {
        id: 34,
        subject: 'Git',
        question: 'TEST',
        answer: null,
        comments: '',
    },
    {
        id: 35,
        subject: 'Git',
        question: 'TESTTEST',
        answer: null,
        comments: '',
    },
    {
        id: 36,
        subject: 'NodeJs',
        question: 'TESTTESTTESTTESTTEST',
        answer: null,
        comments: '',
    },
    {
        id: 37,
        subject: 'NodeJs',
        question: 'TESTTESTTEST',
        answer: null,
        comments: '',
    },
    {
        id: 38,
        subject: 'NodeJs',
        question: 'TESTTESTTEST',
        answer: null,
        comments: '',
    },
    {
        id: 39,
        subject: 'NodeJs',
        question: 'TEST',
        answer: null,
        comments: '',
    },
    {
        id: 40,
        subject: 'NodeJs',
        question: 'TESTTESTTESTTESTTEST',
        answer: null,
        comments: '',
    },
]