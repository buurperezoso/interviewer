El proyecto esta hecho con ReactJs usando Next.js para hacer el server-side-render. El manejo de los datos globales fue usando redux con react-redux y el API que esta usando esta hecho en Serverless y esta montado en AWS usando API gateways, lambda y dynamoDB para la base de datos.

##### Se pone un poco lento el cambio de vistas cuando es la primera vez que entra :c

Por si salen problemas del CORS:
https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf?hl=es


El servidor local es: http://localhost:3000/

Y la ruta para el demo del proyecto es: https://interviewer.vercel.app/
