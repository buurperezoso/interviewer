import { api } from '../constants/constants';

const httpRequestHandler = async (route, method, payload = null) => {

    const url = `${api}/${route}`;
    let options, response;

    if (method !== 'GET') {
        options = {
            method: method,
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
            },
            crossDomain: true,
            body: JSON.stringify(payload),
        };
    } else {
        options = {
            method: method,
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
            },
            crossDomain: true,
        };
    }

    try {
        const res = await fetch(url, options);
        response = await res.json();
    } catch (error) {
        console.error(error);
    }

    return response;
}

export default httpRequestHandler;