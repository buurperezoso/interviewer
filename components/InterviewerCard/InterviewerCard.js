import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row } from "react-bootstrap";

// Styles
import styles from './InterviewerCard.module.css';

const InterviewerCard = ({ name, employeeID, enterpriseID }) => {
    return (
        <Row className='text-center'>
            <Col xs={12}>
                <FontAwesomeIcon className={styles.titleIcon} icon={['far', 'user']} />
            </Col>
            <Col xs={12} className='mt-3'>
                <span className={styles.name}>{name}</span>
            </Col>
            <Col xs={12} className='mt-1'>
                <span>#{employeeID}</span>
            </Col>
            <Col xs={12} className='mt-1'>
                <span className={styles.idEnterprise}>{enterpriseID}</span>
            </Col>
        </Row>
    );
}

export default InterviewerCard;