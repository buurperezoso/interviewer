import { Fragment } from 'react';
import { Col, Container, Row } from 'react-bootstrap';

// Styles
import styles from './Header.module.css';

const Header = ({ title }) => {
    return (
        <Fragment>
            <div className={styles.background}></div>
            <Container>
                <Row>
                    <Col xs={12} className='pl-2 pl-md-0 mt-2'>
                        <span className={styles.title}>{title}</span>
                    </Col>
                </Row>
            </Container>
        </Fragment>
    );
}

export default Header;