import { useCallback, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconButton, Input, InputAdornment, InputLabel, TextField, FormControl } from "@material-ui/core";
import { Visibility, VisibilityOff } from '@material-ui/icons';
import Link from "next/link";
import { Col, Row } from "react-bootstrap";
import { useRouter } from 'next/router';

// Styles
import styles from './Login.module.css';

// Components
import DialogTitle from '../DialogTitle/DialogTitle';

const Login = () => {

    const router = useRouter();

    const [showPassword, setShowPassword] = useState(false);

    const handleMouseDownPassword = useCallback((event) => {
        event.preventDefault();
    }, []);

    return (
        <Row className='h-100 align-items-center'>
            <Col xs={11} md={7} lg={5} className={styles.cardContainer}>
                <Row>

                    <DialogTitle title='Iniciar sesión' />

                    <Col xs={12} className='my-4'>
                        <TextField
                            // error={titleError}
                            fullWidth
                            label="Usuario"
                            placeholder='Ingrese el nombre del usuario'
                        // value={title}
                        // onChange={e => {
                        //     setTitle(e.target.value);
                        //     if (e.target.value) {
                        //         setTitleError(false);
                        //     } else {
                        //         setTitleError(true);
                        //     }
                        // }}
                        // helperText={titleError && 'This field is requiered.'}
                        />
                    </Col>
                    <Col xs={12}>
                        <FormControl fullWidth>
                            <InputLabel>Contraseña</InputLabel>
                            <Input
                                // error={titleError}
                                fullWidth
                                label="Nombre del usuario"
                                placeholder='Ingrese la contraseña'
                                type={showPassword ? 'text' : 'password'}
                                // value={title}
                                // onChange={e => {
                                //     setTitle(e.target.value);
                                //     if (e.target.value) {
                                //         setTitleError(false);
                                //     } else {
                                //         setTitleError(true);
                                //     }
                                // }}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={() => setShowPassword(!showPassword)}
                                            onMouseDown={handleMouseDownPassword}
                                        >
                                            {showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            // helperText={titleError && 'This field is requiered.'}
                            />
                        </FormControl>
                    </Col>
                    <Col xs={12} className='mt-5 text-right'>
                        <button type='submit' className='btn btn-primary rounded-0 px-4' onClick={() => router.push('/home')}>
                            <span>Login</span>
                        </button>
                    </Col>
                    <Col xs={12} className='mt-2 text-center'>
                        <Link href='/sign-up'>¿Aún no tienes cuenta?</Link>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
}

export default Login;