import { Fragment, useEffect } from "react";

// Redux
import { useDispatch, useSelector } from "react-redux";
import allActions from '../../redux/actions';

// Http requests
import httpRequestHandler from "../../httpRequest";

// Components
import Loader from "../Loader/Loader";


const GetInterviewsComponent = ({ children }) => {

    const loading = useSelector(state => state.loadingReducer.loading);
    const dispatch = useDispatch();

    useEffect(async () => {

        dispatch(allActions.loadingActions.startLoader());

        let interviews;

        try {
            interviews = await httpRequestHandler('get-interviews', 'GET');
            if (interviews) {
                dispatch(allActions.interviewActions.setInterviewList(interviews));
            }
            dispatch(allActions.loadingActions.stopLoader());
        } catch (error) {
            dispatch(allActions.loadingActions.stopLoader());
        }

    }, []);

    return (
        <Fragment>
            { loading ? <Loader /> : null}
            {children}
        </Fragment>
    )
}



export default GetInterviewsComponent;