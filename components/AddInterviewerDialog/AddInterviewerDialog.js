import { useCallback, useState, useEffect } from "react";
import { Dialog, TextField } from "@material-ui/core";
import { Container, Row, Col } from "react-bootstrap";

// Redux
import { useDispatch } from "react-redux";
import allActions from '../../redux/actions';

// Http requests
import httpRequestHandler from '../../httpRequest';

// Components
import DialogTitle from "../DialogTitle/DialogTitle";

const AddInterviewerDialog = ({ info, handleClose }) => {

    // Form values
    const [name, setName] = useState('');
    const [employeeID, setEmployeeID] = useState('');
    const [enterpriseID, setEnterpriseID] = useState('');

    // Form errors
    const [nameError, setNameError] = useState(false);
    const [employeeIDError, setEmployeeIDError] = useState(false);
    const [enterpriseIDError, setEnterpriseIDError] = useState(false);

    const dispatch = useDispatch();

    const resetForm = () => {

        setName('');
        setEmployeeID('');
        setEnterpriseID('');

        setNameError(false);
        setEmployeeIDError(false);
        setEnterpriseIDError(false);

    };

    useEffect(() => {

        if (info.profile) {
            const { name, employeeID, enterpriseID } = info.profile;
            setName(name);
            setEmployeeID(employeeID);
            setEnterpriseID(enterpriseID);
        }

    }, [info]);

    const saveProfile = async (profile) => {

        dispatch(allActions.loadingActions.startLoader());
        try {
            const response = await httpRequestHandler(`add-profile`, 'POST', profile);
            dispatch(allActions.profileActions.addProfile(response));
            dispatch(allActions.loadingActions.stopLoader());
            handleClose({
                open: false,
                profile: null
            });
            resetForm();

        } catch (error) {
            console.error(error);
            dispatch(allActions.loadingActions.stopLoader());
        }

    }

    const updateProfile = async (profile, profileID) => {

        dispatch(allActions.loadingActions.startLoader());
        try {
            const response = await httpRequestHandler(`update-profile/${profileID}`, 'POST', profile);
            dispatch(allActions.profileActions.updateProfile(response));
            dispatch(allActions.loadingActions.stopLoader());
            handleClose({
                open: false,
                profile: null
            });
            resetForm();

        } catch (error) {
            console.error(error);
            dispatch(allActions.loadingActions.stopLoader());
        }

    }

    const handleSubmit = async (event) => {

        event.preventDefault();

        let error = false;

        if (!name) {
            setNameError(true);
            error = true;
        } else {
            setNameError(false);
        }

        if (!employeeID) {
            setEmployeeIDError(true);
            error = true;
        } else {
            setEmployeeIDError(false);
        }

        if (!enterpriseID) {
            setEnterpriseIDError(true);
            error = true;
        } else {
            setEnterpriseIDError(false);
        }

        if (!error) {

            let profile = {
                name,
                employeeID,
                enterpriseID,
            }

            if (info.profile) {
                await updateProfile(profile, info.profile.id);
            } else {
                await saveProfile(profile);
            }
        }
    };

    return (
        <Dialog
            open={info.open}
            onClose={() => {
                handleClose({
                    open: false,
                    profile: null
                });
                resetForm();
            }}
        >
            <form autoComplete='off' onSubmit={handleSubmit}>
                <Container className='py-4 px-sm-5'>
                    <Row>
                        <DialogTitle
                            title={info.profile ? 'Editar entrevistador' : 'Nuevo entrevistador'}
                            icon='userPlus'
                        />

                        <Col xs={12} className='mt-5'>
                            <TextField
                                error={nameError}
                                fullWidth
                                label="Nombre completo"
                                placeholder='Ingrese el nombre completo del usuario'
                                value={name}
                                onChange={e => {
                                    setName(e.target.value);
                                    if (e.target.value) {
                                        setNameError(false);
                                    } else {
                                        setNameError(true);
                                    }
                                }}
                                helperText={nameError && 'This field is requiered.'}
                            />
                        </Col>
                        <Col xs={12} className='mt-4'>
                            <TextField
                                error={employeeIDError}
                                fullWidth
                                label="Id del empleado"
                                placeholder='Ingrese el id del empleado'
                                value={employeeID}
                                onChange={e => {
                                    const onlyNums = e.target.value.replace(/[^0-9]/g, '');
                                    setEmployeeID(onlyNums);
                                    if (onlyNums) {
                                        setEmployeeIDError(false);
                                    } else {
                                        setEmployeeIDError(true);
                                    }
                                }}
                                helperText={employeeIDError && 'This field is requiered.'}
                            />
                        </Col>
                        <Col xs={12} className='mt-4'>
                            <TextField
                                error={enterpriseIDError}
                                fullWidth
                                label="EID"
                                placeholder='Ingrese el EID'
                                value={enterpriseID}
                                onChange={e => {
                                    setEnterpriseID(e.target.value);
                                    if (e.target.value) {
                                        setEnterpriseIDError(false);
                                    } else {
                                        setEnterpriseIDError(true);
                                    }
                                }}
                                helperText={enterpriseIDError && 'This field is requiered.'}
                            />
                        </Col>
                        <Col xs={12} className='mt-5 text-right'>
                            <button className='btn btn-primary rounded-0 px-4'>Guardar</button>
                        </Col>
                    </Row>
                </Container>
            </form>
        </Dialog>
    );
}

export default AddInterviewerDialog;