import { useCallback, useState, useEffect } from "react";
import { Checkbox, Dialog, FormControlLabel, TextField } from "@material-ui/core";
import { Col, Container, Row } from "react-bootstrap";
import { useRouter } from 'next/router';

// Redux
import { useSelector, useDispatch } from "react-redux";
import allActions from '../../redux/actions';

// Http requests
import httpRequestHandler from '../../httpRequest';

// Components
import DialogTitle from "../DialogTitle/DialogTitle";

// Constants
import { skillsPreset } from '../../constants/constants';


const AddSkillsDialog = ({ open, handleClose }) => {

    const router = useRouter();
    const interviews = useSelector(state => state.interviewReducer.interviews);
    const dispatch = useDispatch();

    const [skills, setSkills] = useState(skillsPreset);

    useEffect(() => { // If skills array got value
        const { id } = router.query;
        const interview = interviews.find((interview) => interview.id == id);
        if (interview && interview.skills.length > 0) {
            setSkills(interview.skills);
        }
    }, [interviews]);

    const handleChange = (index) => {

        const newSkills = skills.map((skill, indexSkils) => {
            if (indexSkils === index) {
                skill.active = !skill.active;
            }

            return skill;
        });

        setSkills(newSkills);

    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const { id } = router.query;

        dispatch(allActions.loadingActions.startLoader());

        try {
            const response = await httpRequestHandler(`add-skills/${id}`, 'POST', skills);
            dispatch(allActions.interviewActions.addSkills(response, id));
            handleClose(false);
            setTimeout(() => {
                dispatch(allActions.loadingActions.stopLoader());
            }, 500);
        } catch (error) {
            dispatch(allActions.loadingActions.stopLoader());
            console.error(error);
        }

    }

    return (
        <Dialog
            open={open}
            onClose={() => handleClose(false)}
            disableBackdropClick={true}
            disableEscapeKeyDown={true}
        >
            <form autoComplete='off' onSubmit={handleSubmit}>
                <Container className='py-4 px-sm-5'>
                    <Row>

                        <DialogTitle
                            title='Skills a evaluar:'
                            icon='skills'
                        />

                        <Col xs={12} className='mt-4'></Col>

                        {
                            skills.map(({ name, active }, index) => {
                                return (
                                    <Col xs={12} md={6} key={index}>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={active}
                                                    name={name.toUpperCase()}
                                                    onChange={() => handleChange(index)}
                                                />
                                            }
                                            label={name}
                                        />
                                    </Col>
                                )
                            })
                        }

                    </Row>
                    <Row>
                        <Col xs={6} className='mt-5 text-left'>
                            {
                                interviews.find((interview) => interview.id == router.query.id) ? (
                                    interviews.find((interview) => interview.id == router.query.id).skills.length === 0 ? (
                                        <button
                                            type='button'
                                            className='btn btn-secondary rounded-0 px-4'
                                            onClick={() => handleClose(false)}
                                        >
                                            Cancelar
                                        </button>
                                    ) : null
                                ) : null
                            }
                        </Col>
                        <Col xs={6} className='mt-5 text-right'>
                            <button
                                className='btn btn-primary rounded-0 px-4'
                                disabled={skills.find((skill) => skill.active) ? false : true}
                            >
                                Guardar
                        </button>
                        </Col>
                    </Row>
                </Container>
            </form>

        </Dialog>
    );
}

export default AddSkillsDialog;