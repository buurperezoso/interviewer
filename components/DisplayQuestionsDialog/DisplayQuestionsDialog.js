import { forwardRef, useEffect, useRef, useState } from "react";
import { Dialog, Slide, Tooltip } from "@material-ui/core";
import { DataGrid } from '@material-ui/data-grid';
import { useRouter } from "next/router";
import { Col, Container, Row } from "react-bootstrap"

// Redux
import { useSelector } from "react-redux";

// Styles
import styles from './DisplayQuestionsDialog.module.css';

// Components
import DialogTitle from "../DialogTitle/DialogTitle";

const Transition = forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const DisplayQuestionsDialog = ({ open, handleClose }) => {

    const [questions, setQuestions] = useState(null);
    const router = useRouter();
    const timer = useRef(null);
    const interviews = useSelector(state => state.interviewReducer.interviews);

    useEffect(() => {
        const { id } = router.query;
        const interview = interviews.find((interview) => interview.id == id);

        if (interview && 'questions' in interview) {
            setQuestions(interview.questions);
        }

    }, [interviews]);

    const columns = [
        {
            field: 'subject',
            headerName: 'Tema',
            width: 150
        },
        {
            field: 'question',
            headerName: 'Pregunta',
            width: 500,
            renderCell: (params) => (
                <Tooltip title={params.value}>
                    <span className={styles.tableCellTrucate}>{params.value}</span>
                </Tooltip>
            ),
        },
        {
            field: 'answer',
            headerName: 'Respuesta',
            width: 130
        },
        {
            field: 'comments',
            headerName: 'Comentarios',
            width: 260,
            renderCell: (params) => (
                <Tooltip title={params.value}>
                    <span className={styles.tableCellTrucate}>{params.value}</span>
                </Tooltip>
            ),
        },
    ];

    return (
        <Dialog
            open={open}
            onClose={() => handleClose(false)}
            fullScreen
            TransitionComponent={Transition}
        >
            <Container className='py-4 px-sm-5'>
                <Row>
                    <Col xs={12} className='text-right'>
                        <button type='button' className='btn btn-secondary' onClick={() => handleClose(false)}>Close</button>
                    </Col>
                    <DialogTitle
                        title='Preguntas'
                        icon='question'
                    />

                    <Col xs={12} className='mt-5'>
                        <div style={{ height: '70vh' }}>
                            {
                                questions ? (
                                    <DataGrid
                                        rows={questions}
                                        columns={columns}
                                        pageSize={8}
                                        autoHeight={true}
                                        checkboxSelection={false}
                                    />
                                ) : null
                            }
                        </div>
                    </Col>
                </Row>
            </Container>
        </Dialog>
    )
}

export default DisplayQuestionsDialog;