import { useEffect, useState } from "react";
import { Dialog, FormHelperText, InputLabel, MenuItem, Select, TextField, FormControl } from "@material-ui/core";
import { useRouter } from "next/router";
import { Container, Row, Col } from "react-bootstrap";

// Redux
import { useDispatch, useSelector } from "react-redux";
import allActions from '../../redux/actions';

// Http requests
import httpRequestHandler from "../../httpRequest";

// Components
import DialogTitle from "../DialogTitle/DialogTitle";

const AddCandidateDialog = ({ open, handleClose }) => {

    const router = useRouter();
    const dispatch = useDispatch();

    const interviews = useSelector(state => state.interviewReducer.interviews);

    // Form values
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [type, setType] = useState('');

    // Form errors
    const [nameError, setNameError] = useState(false);
    const [emailError, setEmailError] = useState(false);
    const [typeError, setTypeError] = useState(false);

    const [candidate, setCandidate] = useState(null);

    const resetForm = () => {
        setName('');
        setEmail('');
        setType('');

        setNameError(false);
        setEmailError(false);
        setTypeError(false);
    }

    useEffect(() => { // If candidate object in interview got value set them in the form
        const { id } = router.query;
        const interview = interviews.find((interview) => interview.id == id);
        if (interview && interview.candidate) {
            setCandidate(interview.candidate);

            setName(interview.candidate.name);
            setEmail(interview.candidate.email);
            setType(interview.candidate.type);
        }

    }, [interviews, open]);

    const handleSubmit = async (event) => {

        event.preventDefault();

        let error = false;

        if (!name) {
            setNameError(true);
            error = true;
        } else {
            setNameError(false);
        }

        if (!email) {
            setEmailError(true);
            error = true;
        } else {
            setEmailError(false);
        }

        if (!type) {
            setTypeError(true);
            error = true;
        } else {
            setTypeError(false);
        }

        if (!error) {

            await saveCandidate();

        }

    };

    const saveCandidate = async () => {
        const { id } = router.query;

        if (candidate) {

            const newCandidate = {
                id: candidate.id,
                name,
                email,
                type,
            }

            dispatch(allActions.loadingActions.startLoader());

            try {

                const response = await httpRequestHandler(`add-candidate/${id}`, 'POST', newCandidate);
                dispatch(allActions.interviewActions.addCandidate(response, id));
                dispatch(allActions.loadingActions.stopLoader());
                handleClose(false);
                resetForm();

            } catch (error) {
                dispatch(allActions.loadingActions.stopLoader());
                console.error(error);
            }


        } else {

            const newCandidate = {
                name,
                email,
                type,
            }

            dispatch(allActions.loadingActions.startLoader());

            try {

                const response = await httpRequestHandler(`add-candidate/${id}`, 'POST', newCandidate);
                dispatch(allActions.interviewActions.addCandidate(response, id));
                dispatch(allActions.loadingActions.stopLoader());
                handleClose(false);
                resetForm();

            } catch (error) {
                dispatch(allActions.loadingActions.stopLoader());
                console.error(error);
            }

        }
    }

    return (
        <Dialog
            open={open}
            onClose={() => {
                handleClose(false);
                resetForm();
            }}
        >
            <form autoComplete='off' onSubmit={handleSubmit}>
                <Container className='py-4 px-sm-5'>
                    <Row>
                        <DialogTitle
                            title='Datos del candidato'
                            icon='userPlus'
                        />

                        <Col xs={12} className='mt-5'>
                            <TextField
                                error={nameError}
                                fullWidth
                                label="Nombre completo"
                                placeholder='Ingrese el nombre completo del candidato'
                                value={name}
                                onChange={e => {
                                    setName(e.target.value);
                                    if (e.target.value) {
                                        setNameError(false);
                                    } else {
                                        setNameError(true);
                                    }
                                }}
                                helperText={nameError && 'This field is requiered.'}
                            />
                        </Col>
                        <Col xs={12} className='mt-4'>
                            <TextField
                                error={emailError}
                                fullWidth
                                label="Correo electrónico"
                                placeholder='Ingrese el correo electrónico del candidato'
                                value={email}
                                onChange={e => {
                                    setEmail(e.target.value);
                                    if (e.target.value) {
                                        setEmailError(false);
                                    } else {
                                        setEmailError(true);
                                    }
                                }}
                                helperText={emailError && 'This field is requiered.'}
                            />
                        </Col>
                        <Col xs={12} className='mt-4'>
                            <FormControl fullWidth>
                                <InputLabel id="type" error={false}>Tipo</InputLabel>
                                <Select
                                    error={typeError}
                                    labelId="type"
                                    value={type}
                                    onChange={e => {
                                        setType(e.target.value);
                                        if (e.target.value) {
                                            setTypeError(false);
                                        } else {
                                            setTypeError(true);
                                        }
                                    }}
                                >
                                    <MenuItem value='Interno'>Interno</MenuItem>
                                    <MenuItem value='Externo'>Externo</MenuItem>

                                </Select>
                                {   // Error message
                                    typeError && <FormHelperText style={{ color: 'red' }}>This field is requiered.</FormHelperText>
                                }
                            </FormControl>

                        </Col>
                        <Col xs={12} className='mt-5 text-right'>
                            <button className='btn btn-primary rounded-0 px-4'>Guardar</button>
                        </Col>
                    </Row>
                </Container>
            </form>
        </Dialog>
    );
}

export default AddCandidateDialog;