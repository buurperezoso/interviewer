import { Dialog } from "@material-ui/core";
import { Col, Container, Row } from "react-bootstrap";

// Components
import DialogTitle from "../DialogTitle/DialogTitle";

const DeleteDialog = ({ info, handleClose, confirm }) => {
    return (
        <Dialog
            open={info.open}
            onClose={() => { handleClose({ id: null, open: false }) }}
        >
            <Container className='py-4 px-sm-5'>

                <DialogTitle
                    title='Eliminar'
                    icon='delete'
                    fullWidth
                />

                <Row>
                    <Col xs={12} className='mt-4'>
                        <span style={{ fontSize: '1.4rem' }}>¿Esta seguro que desea eliminar este elemento?</span>
                    </Col>
                </Row>
                <Row className='mt-5'>
                    <Col xs={6}>
                        <button type='button' className='btn btn-secondary rounded-0' onClick={() => handleClose({ id: null, open: false })}>
                            Cancelar
                        </button>
                    </Col>
                    <Col xs={6} className='text-right'>
                        <button type='button' className='btn btn-danger rounded-0' onClick={() => confirm(info.id)}>
                            Eliminar
                        </button>
                    </Col>
                </Row>
            </Container>
        </Dialog>
    );
}

export default DeleteDialog;