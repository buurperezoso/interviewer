import Lottie from 'react-lottie';
import animationData from '../../assets/8220-loding.json';

// Styles
import styles from './Loader.module.css';

const Loader = () => {

    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };

    return (
        <div className={styles.loader}>

            <Lottie style={{ cursor: 'default' }} options={defaultOptions}
                height={400}
                width={400}
                isClickToPauseDisabled={true}
            />

        </div>
    );
}

export default Loader;