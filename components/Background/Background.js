import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

// Styles
import styles from './Background.module.css';

// Components
import Header from "../Header/Header";

const Background = ({
    children,
    title,
    background, // Boolean {true} shows gray background
    disabled, // Boolean {true} disables the continue button
    showContinue, // Boolean {true} shows the continue button and {false} hide it
    clickContinue, // Function send as param triggered by continue button
    backButton, // Function send as param triggered by continue button
    label // Continue button label
}) => {

    const showContinueButton = showContinue ? showContinue : false;

    return (
        <div className='background' style={{ backgroundColor: background ? 'rgb(207, 207, 207)' : 'white' }}>
            <Header title={title} />

            {children}

            {
                showContinueButton &&
                <button
                    className={`btn btn-primary rounded-0 px-4 ${styles.continueButton}`}
                    onClick={clickContinue}
                    disabled={disabled}
                >
                    <span>{label ? label : 'Continuar'}</span>
                    <FontAwesomeIcon className='ml-2' icon={['fas', 'arrow-right']} />
                </button>

            }

            {
                backButton ? (
                    <button
                        className={`btn btn-orange rounded-0 px-4 ${styles.backButton}`}
                        onClick={backButton}
                    >
                        <FontAwesomeIcon icon={['fas', 'arrow-left']} />
                        <span className='ml-2'>Regresar</span>
                    </button>
                ) : null
            }

        </div>
    );
}

export default Background;