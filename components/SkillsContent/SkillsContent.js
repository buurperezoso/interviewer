import { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row } from "react-bootstrap";

// Styles
import styles from '../../pages/candidate/Candidate.module.css';

const SkillsContent = ({ skills, openDialog }) => {

    const [skillsList, setSkillsList] = useState([]);

    useEffect(() => {
        setSkillsList(skills);
    }, [skills, setSkillsList]);

    return (
        <Col xs={12} md={6} className='mt-3 mt-md-5'>
            <div className={styles.borderFull} onClick={() => openDialog(true)}>
                <Row>
                    <Col xs={12}>
                        <span style={{ fontSize: '2.5rem', fontWeight: '600' }}>Skills a evaluar:</span>
                    </Col>
                </Row>
                <Row>
                    {
                        skillsList.map(({ name, active }, index) => {
                            if (active) {
                                return (
                                    <Col xs={12} className='mt-2' key={index}>
                                        <FontAwesomeIcon icon={['fas', 'circle']} />
                                        <span style={{ fontSize: '1.5rem', marginLeft: '1rem' }}>{name}</span>
                                    </Col>
                                )
                            }
                        })
                    }
                </Row>
            </div>
        </Col>
    );
}

export default SkillsContent;