import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col, Row } from "react-bootstrap";

// Syles
import styles from '../../pages/candidate/Candidate.module.css';

const NoSkillsContent = ({ openDialog }) => {
    return (
        <Col xs={12} md={6} className='mt-3 mt-md-5'>
            <div className={styles.borderFull} onClick={() => openDialog(true)}>
                <Row>
                    <Col xs={12}>
                        <span style={{ fontSize: '2.5rem', fontWeight: '600' }}>Skills a evaluar:</span>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} className='text-center mt-4'>
                        <span style={{ fontSize: '1.2rem' }}>No se han seleccionado skills</span>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} className='text-center mt-4'>
                        <FontAwesomeIcon className={styles.skillsIcon} icon={['fas', 'code']} />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} className='text-center mt-4'>
                        <span style={{ fontSize: '1.2rem' }}>Haz click para añadir</span>
                    </Col>
                </Row>
            </div>
        </Col>
    );
}

export default NoSkillsContent;