import { Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Col } from "react-bootstrap";

// Styles
import styles from './DialogTitle.module.css';

const DialogTitle = ({ title, icon }) => {
    return (
        <Col xs={12}>
            {(() => {
                switch (icon) {
                    case 'userPlus':
                        return (
                            <Fragment>
                                <FontAwesomeIcon className={styles.titleIcon} icon={['far', 'user']} />
                                <div className={styles.plusIconContainer}>
                                    <FontAwesomeIcon className={styles.plusIcon} icon={['fas', 'plus']} />
                                </div>
                            </Fragment>
                        );
                    case 'skills':
                        return <FontAwesomeIcon className={styles.titleIcon} icon={['fas', 'code']} />;
                    case 'question':
                        return <FontAwesomeIcon className={styles.titleIcon} icon={['fas', 'question']} />;
                    case 'delete':
                        return <FontAwesomeIcon className={styles.titleIcon} icon={['fas', 'trash-alt']} />;
                    default:
                        return <FontAwesomeIcon className={styles.titleIcon} icon={['far', 'user']} />;
                }
            })()}
            <span className={styles.title}>{title}</span>
        </Col>
    );
}

export default DialogTitle;